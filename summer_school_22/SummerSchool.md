# Palabos Summer School 2022

- [Program of the summer school](Program.md)
- [Documentation: Palabos on GPU](doc/overview.md)

## Practice sessions
- [Session 2 (Monday 11:00-12:00)](session2/Readme.md)
- [Session 3 (Monday 13:30-15:00)](session3/Readme.md)
- [Session 4 (Monday 15:15-17:00)](session4/Readme.md)
- [Session 5 (Monday 9:00-10:30)](session5/Readme.md)
- [Session 6 (Monday 11:00-12:00)](session6/Readme.md)
