# Pore-level flow through a Berea sandstone

Simulates a single-phase flow through a fully resolved porous media. A 400x400x400 Berea sandstone is used as a model and replicated periodically to fill a larger space.

## Usage
Compilation:

    cd build
    export CXX=nvc++
    make -j

Execution:

- Before running, put the file "Berea.ascii" in the execution directory. File can be downloaded at https://www.dropbox.com/s/6mf545fva4e7hf2/Berea.ascii?dl=0
- Execute in benchmark mode:  `mpirun -np numNodes ./sandstone 1 [nx] [ny] [nz]`
- Execute in production mode (produces VTK files):  `mpirun -np numNodes ./sandstone 0 [nx] [ny] [nz]`

