## Session 3: First Palabos-GPU applications

For this exercise, change into the `exercise3` directory and have a look at the two provided examples: the lid-driven flow in a cavity and the flow through a porous media. For the porous media example, you need to download the geometry:

    wget https://www.dropbox.com/s/6mf545fva4e7hf2/Berea.ascii?dl=0
    mv Berea* Berea.ascii

Before compiling the cavity and porous media exampes, spot the places commented with `TODO` and fill out the missing lines and/or try to answer the questions. Then, compile, execute, observe.
