## Accelerating collision models

The accelerated lattice makes no use of virtual function calls to select the cell-based collision model. Instead, the collision-streaming function on a cell goes through a sort of switch statement to exect the proper collision function from a local ID. All class-based features of the dynamics obejct (including static class variables and object variables) are properly translated from and to the accelerated lattice. Class variables (for example the magic constants of the TRT model) are now treated as so-called static variables and attached to the acceleratedBlock. Dynamic variables (for example the relaxation time) are listed in a separate array, to which the collision function receives an index.

If you want your own dynamics class to work well on GPU, you need to provide Palabos with sufficient information to carry out all these conversions. 
### Define data conversion between accelerated and multi-block lattice

Open the file `src/acceleratedLattice/acceleratedDefinitions.h` and add the following data:

- Define a constant for your dynamics class in the namespace `CollisionModel`. For composite dynamics, a different constant must be defined for everey combination (e.g. Smagorinsky + BGK, Smagorinsky + TRT, etc.)
- Link your constant to the string assigned for your dynamics class (see "Find the string assigned to a dynamics object by Palabos' introspection system" below).
- Define the number of dynamic scalars in your dynamics class, and write function which extract static and dynamic scalars from your class and class instances.

**Find the string assigned to a dynamics object by Palabos' introspection system**. You can find the string you are searching for with the following piece of code:

    template<typename T, template<typename U> class Descriptor>
    std::string getStringID(Dynamics<T, Descriptor> const& dynamics) {
        std::vector<int> dynamicsChain;
        constructIdChain(dynamics, dynamicsChain);
        return meta::constructIdNameChain<T,Descriptor>(dynamicsChain, " >> ");
    }

### Write the collision function for your collision model

The `collide` function of your dynamics object cannot be called from the GPU, you need to write out its content again (ideally by calling the same template functions). Go to `src/acceleratedLattice/acceleratedCollisionModels.h` and writ the corresponding `collide` function, imitating the syntax of the existing ones.

You should also edit `acceleratedCollisions.h` and list your model in the function `collide` to make sure it is included in the general, non-specialized kernel.

- [Content](overview.md)
- [Previous: Running an accelerated application](running-an-accelerated-application.md)
- [Next: Accelerating data processors](accelerating-data-processors.md)
