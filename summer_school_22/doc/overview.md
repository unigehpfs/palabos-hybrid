# Documentation: Palabos on GPU

Content of the documentation:

- [Running an accelerated application](running-an-accelerated-application.md)
- [Accelerating collision models](accelerating-collision-models.md)
- [Accelerating data processors](accelerating-data-processors.md)
