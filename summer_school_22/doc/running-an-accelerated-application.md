## Running an accelerated application

To write a Palabos application that runs on an accelerated GPU (so far, that's essentially an NVIDIA GPU), your fundamental data structure is the `AcceleratedLattice3D`, which replaces the `MultiBlockLattice3D`. The accelerated lattice uses a different data layout (structure-of-array instead of array-of-structure, storage of class variables in a data-oriented fashion, etc.). You could simply forget about the multi-block lattice and write everything with the accelerated lattice alone. However, not all Palabos functionality has been ported to the accelerated lattice at this stage. It is therefore more convenient to instantiate both a multi-block lattice (which handles pre- and post-processing stages, mostly), and an accelerated lattice which handles the collision-streaming cycles efficiently. Copying data back and forth between the two is easy.

An accelerated application respects the following principles:

**Blocking communictation**. At the current stage, multi-GPU application support blocking communication only. Therefore, you application (if it is going to use MPI) should start with something like:

    plbInit(&argc, &argv);
    defaultMultiBlockPolicy().toggleBlockingCommunication(true);

**Instantiation of an accelerated lattice**. Once your program has created a multi-block lattice through a line like

    MultiBlockLattice3D<T, DESCRIPTOR> lattice(nx, ny, nz, new TRTdynamics<T,DESCRIPTOR>(1. / tau) ); 

and has gone through initialization stages, you can copy all data into an accelerated lattice:

    AcceleratedLattice3D<T, DESCRIPTOR> accLattice(lattice);

**Collision-streaming cycles**. Now, simply run your collision-streaming cycles on the accelerated lattice:

    accLattice.collideAndStream();

**Post-processing**. For post-processing, either perform your post-processing operations directly on the accelerated lattice (if they are implemented, or if you have written your own data processors), or write the data back to the multi-block lattice:

    accLattice.writeBack(lattice)

before doing post-processing on CPU as usual.

**Speeding up collision-streaming**. The collision-streaming GPU kernel can be accelerated if you hand-pick the collision models that are compiled into the kernel. This is done by replacing the original command through something like the following:

    accLattice.collideAndStream();
    accLattice.collideAndStream (
        CollisionKernel<T,DESCRIPTOR,
                        CollisionModel::TRT,
                        CollisionModel::HalfwayBounceBack__TRT>() );

It may be tricky, though, to figure out which collision models are needed, or how they are named. You can however print out the proper `collideAndStream` command for a given lattice, at run time, through the following line:

    showTemplateArguments(lattice);

applied to the original multi-block lattice. You then need to copy the printed command back into your code, and recompile.

- [Content](overview.md)
- [Next: Accelerating collision models](accelerating-collision-models.md)

